/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import {
    Button, Tab, Tabs, TextField,
} from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useState } from 'react';
import {
    ApolloClient, InMemoryCache, ApolloProvider, split, createHttpLink, gql, useSubscription, useQuery,
} from '@apollo/client';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { setContext } from '@apollo/client/link/context';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import ReactJson from 'react-json-view';
import {
    Link, Route, Routes, useLocation,
} from 'react-router-dom';
import Apps from './pages/Apps';
import Devices from './pages/Devices';
import Instances from './pages/Instances';
import Rqlite from './pages/rqlite';
import Users from './pages/Users';
import Volumes from './pages/Volumes';
import AppPage from './pages/App';
import Instance from './pages/Instance';
import { server } from './api';
import Events from './pages/Events';

import logo from './logo.svg';

const httpLink = createHttpLink({
    uri: `http://${server}`,
});

const wsLink = new WebSocketLink(
    new SubscriptionClient(`ws://${server}`, {
        connectionParams: {
            token: localStorage.getItem('megapolos.token'),
        },
        reconnect: true,
    }),
);

const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem('megapolos.token');
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            token,
        },
    };
});

const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
        );
    },
    wsLink,
    authLink.concat(httpLink),
);

const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
});

const theme = createTheme({
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    margin: '0px 10px',
                },
            },
            defaultProps: {
                size: 'small',
                variant: 'contained',
            },
        },
    },
});

function Login(props) {
    const [token, setToken] = useState('');
    return <div>
        <TextField value={token} onChange={e => setToken(e.target.value)} variant="standard" />
        <Button onClick={() => {
            localStorage.setItem('megapolos.token', token);
            props.refetch();
        }}
        >
Login
        </Button>
    </div>;
}

function Logout(props) {
    return <div style={{
        padding: '0px 10px',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        boxSizing: 'border-box',
    }}
    >
        <img src={logo} alt="logo" style={{ width: '100px' }} />
        <div>
            {props.user?.name}
            <Button onClick={() => {
                localStorage.removeItem('megapolos.token');
                props.refetch();
            }}
            >
Logout
            </Button>
        </div>
    </div>;
}

function AppContent() {
    const location = useLocation();
    const {
        loading, error, data, refetch,
    } = useQuery(gql`query {
        getMe {
            id
            name
        }
    }`);

    const [events, setEvents] = useState([]);
    useSubscription(gql`subscription {
        event {
            type
            data
        }
    }`, {
        onData: _data => {
            // console.log(data.data.data.event);
            setEvents(e => [_data.data.data.event, ...e]);
        },
    });

    const pages = {
        apps: <Apps />,
        instances: <Instances />,
        users: <Users />,
        devices: <Devices />,
        volumes: <Volumes />,
        events: <Events events={events} />,
        rqlite: <Rqlite />,
    };

    if ((!loading && !data?.getMe) || error) {
        return <Login refetch={refetch} />;
    }

    return (
        <>
            <Logout user={data?.getMe} refetch={refetch} />
            <div style={{ display: 'flex' }}>
                <div>
                    <Tabs orientation="vertical" value={location.pathname.split('/')[1]}>
                        {Object.keys(pages).map(_page => (
                            <Tab
                                key={_page}
                                value={_page}
                                label={_page}
                                LinkComponent={Link}
                                to={`/${_page}`}
                            />
                        ))}
                    </Tabs>
                </div>
                <div style={{ overflow: 'auto', flex: '1' }}>
                    <Routes>
                        {Object.keys(pages).map(_page => (
                            <Route path={_page} element={pages[_page]} />
                        ))}
                        <Route path="/apps/:id" element={<AppPage />} />
                        <Route path="/instances/:id" element={<Instance />} />
                    </Routes>
                </div>
            </div>
        </>
    );
}

function App() {
    return (
        <ApolloProvider client={client}>
            <ThemeProvider theme={theme}>
                <AppContent />
            </ThemeProvider>
        </ApolloProvider>
    );
}

export default App;
