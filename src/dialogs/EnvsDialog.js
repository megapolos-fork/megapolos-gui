/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { gql, useMutation } from '@apollo/client';
import { Delete } from '@mui/icons-material';
import {
    Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, TextField,
} from '@mui/material';
import { useEffect, useState } from 'react';

const EnvsDialog = props => {
    const [open, setOpen] = useState(false);
    const [envs, setEnvs] = useState([]);
    useEffect(
        () => {
            setEnvs(props.envs || []);
        },
        [open],
    );
    const [changeContainerEnvs] = useMutation(gql`
        mutation($id: String!, $envs: [ContainerParameterInput]) {
            changeContainerEnvs(id: $id envs: $envs)
        }
    `);

    return <>
        <Button onClick={() => setOpen(true)}>Change</Button>
        <Dialog open={open} onClose={() => setOpen(false)}>
            <DialogTitle>Envs</DialogTitle>
            <DialogContent>
                {envs.map((env, i) => <div key={i}>
                    <TextField
                        label="Name"
                        value={env.key}
                        onChange={e => {
                            const newEnvs = JSON.parse(JSON.stringify(envs));
                            newEnvs[i].key = e.target.value;
                            setEnvs(newEnvs);
                        }}
                        variant="standard"
                    />
                    <TextField
                        label="Value"
                        value={env.value}
                        onChange={e => {
                            const newEnvs = JSON.parse(JSON.stringify(envs));
                            newEnvs[i].value = e.target.value;
                            setEnvs(newEnvs);
                        }}
                        variant="standard"
                    />
                    <IconButton onClick={() => {
                        const newEnvs = JSON.parse(JSON.stringify(envs));
                        newEnvs.splice(i, 1);
                        setEnvs(newEnvs);
                    }}
                    >
                        <Delete />
                    </IconButton>
                </div>)}
                <Button onClick={() => {
                    const newEnvs = JSON.parse(JSON.stringify(envs));
                    newEnvs.push({ key: '', value: '' });
                    setEnvs(newEnvs);
                }}
                >
Add
                </Button>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpen(false)}>Cancel</Button>
                <Button onClick={async () => {
                    await changeContainerEnvs({
                        variables: {
                            id: props.containerId,
                            envs: envs.map(env => {
                                delete env.__typename;
                                return env;
                            }),
                        },
                    });
                    props.refetch();
                    setOpen(false);
                }}
                >
OK
                </Button>
            </DialogActions>
        </Dialog>
    </>;
};

export default EnvsDialog;
