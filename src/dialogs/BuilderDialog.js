/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { gql, useLazyQuery, useMutation } from '@apollo/client';
import {
    Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField,
} from '@mui/material';
import { useEffect, useState } from 'react';

const BuilderDialog = props => {
    const [loadDevice] = useLazyQuery(gql`
        query($deviceId: String, $containerId: String) {
            getDevice(id: $deviceId) {
                id
                name
                device_type_id
            }
            getDeviceManifest(id: $deviceId) {
                container_aux_fields
            }
            getContainerDeviceAuxOptions(container_id: $containerId, device_id: $deviceId) {
                container_option_value
                device_option_name
            }
            getContainerDeviceEnvOptions(container_id: $containerId device_id: $deviceId) {
                container_env_name
                device_option_name
            }
        }
    `, { variables: { deviceId: props.deviceId, containerId: props.containerId } });
    const [containerDeviceEnvData, setContainerDeviceEnvData] = useState({});
    const [containerDeviceData, setContainerDeviceData] = useState({});
    useEffect(() => {
        loadDevice({
            onCompleted: data => {
                const _data = {};
                data.getDeviceManifest.container_aux_fields.forEach(field => {
                    if (!_data[field]) {
                        _data[field] = '';
                    }
                });
                if (props.update) {
                    data.getContainerDeviceAuxOptions.forEach(d => {
                        _data[d.device_option_name] = d.container_option_value;
                    });
                }
                setContainerDeviceData(_data);
            },
        });
    }, [props.open]);
    const [addDevice] = useMutation(gql`
        mutation($containerId: String, $input: ContainerDeviceInput) {
            addDeviceToContainer(container_id: $containerId, input: $input)
        }
    `);
    // const [setContainerDeviceEnvOptions] = useMutation(gql`
    //     mutation($containerId: String, $deviceId: String, $options: [ContainerDeviceEnvOptionInput]) {
    //         setContainerDeviceEnvOptions(container_id: $containerId, device_id: $deviceId, options: $options)
    //     }
    // `);
    const [setContainerDeviceAuxOptions] = useMutation(gql`
        mutation($containerId: String, $deviceId: String, $options: [ContainerDeviceAuxOptionInput]) {
            setContainerDeviceAuxOptions(container_id: $containerId device_id: $deviceId options: $options)
        }
    `);

    return <Dialog open={props.open} onClose={props.onClose}>
        <DialogTitle>{props.update ? 'Edit domain' : 'Add domain'}</DialogTitle>
        <DialogContent>
            <h4>Fields</h4>
            {Object.keys(containerDeviceData).map(key => <div key={key}>
                {key}
                <TextField
                    value={containerDeviceData[key]}
                    onChange={e => {
                        const newData = JSON.parse(JSON.stringify(containerDeviceData));
                        newData[key] = e.target.value;
                        setContainerDeviceData(newData);
                    }}
                    variant="standard"
                />
            </div>)}
            <h4>Envs</h4>
            {Object.keys(containerDeviceEnvData).map(key => <div key={key}>
                {key}
                <TextField
                    value={containerDeviceEnvData[key]}
                    onChange={e => {
                        const newData = JSON.parse(JSON.stringify(containerDeviceEnvData));
                        newData[key] = e.target.value;
                        setContainerDeviceEnvData(newData);
                    }}
                    variant="standard"
                />
            </div>)}
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose} color="primary">
                Cancel
            </Button>
            <Button
                onClick={async () => {
                    if (!props.update) {
                        await addDevice({
                            variables: {
                                containerId: props.containerId,
                                input: {
                                    id: props.deviceId,
                                    aux_parameters: [],
                                    env_parameters: Object.keys(containerDeviceEnvData).map(key => ({
                                        key,
                                        value: containerDeviceEnvData[key],
                                    })),
                                },
                            },
                        });
                    }
                    // await setContainerDeviceEnvOptions({
                    //     variables: {
                    //         containerId: props.containerId,
                    //         deviceId: props.deviceId,
                    //         options: Object.keys(containerDeviceEnvData).map(key => ({
                    //             key,
                    //             value: containerDeviceEnvData[key],
                    //         })),
                    //     },
                    // });
                    await setContainerDeviceAuxOptions({
                        variables: {
                            containerId: props.containerId,
                            deviceId: props.deviceId,
                            options: Object.keys(containerDeviceData).map(key => ({
                                key,
                                value: containerDeviceData[key],
                            })),
                        },
                    });
                    props.onClose();
                }}
                color="primary"
            >
                Save
            </Button>
        </DialogActions>
    </Dialog>;
};

export default BuilderDialog;
