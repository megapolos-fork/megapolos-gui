/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { gql, useQuery } from '@apollo/client';
import { Checkbox, TextField } from '@mui/material';

const ContainerEnvOptions = props => {
    const { data, onChange, manifest } = props;
    return <div>
        {manifest?.getDeviceManifest?.container_env_fields.map((field, index) => <div key={field}>
            {field}
            <TextField
                value={data[index].value}
                onChange={e => {
                    const newData = JSON.parse(JSON.stringify(data));
                    newData[index].value = e.target.value;
                    onChange(newData);
                }}
                variant="standard"
            />
        </div>)}
    </div>;
};

const ContainerOptions = props => {
    const { data, onChange, manifest } = props;
    return <div>
        {manifest?.getDeviceManifest?.container_aux_fields.map((field, index) => <div key={field}>
            {field}
            <TextField
                value={data[index].value}
                onChange={e => {
                    const newData = JSON.parse(JSON.stringify(data));
                    newData[index].value = e.target.value;
                    onChange(newData);
                }}
                variant="standard"
            />
        </div>)}
    </div>;
};

const InstanceDevice = props => {
    const { device, instanceForm, setInstanceForm } = props;
    const {
        data: manifest,
    } = useQuery(gql`
        query($id: String) {
            getDeviceManifest(id: $id) {
                container_aux_fields
                container_env_fields
            }
        }
    `, { variables: { id: props.device.id } });

    const checked = instanceForm.devices.find(d => d.id === device.id);

    return <>
        {device.name}
        <Checkbox
            id={device.id}
            value={device.id}
            onChange={e => {
                const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                if (e.target.checked) {
                    newInstanceForm.devices.push({
                        id: device.id,
                        env_parameters: manifest?.getDeviceManifest?.container_env_fields.map(field => ({ key: field, value: '' })),
                        parameters: manifest?.getDeviceManifest?.container_aux_fields.map(field => ({ key: field, value: '' })),
                    });
                    setInstanceForm(newInstanceForm);
                } else {
                    newInstanceForm.devices = newInstanceForm.devices.filter(d => d.id !== device.id);
                    setInstanceForm(newInstanceForm);
                }
            }}
            variant="standard"
        />
        {checked && <div>
Env options
            <ContainerEnvOptions
                manifest={manifest}
                device={device}
                data={instanceForm.devices.find(d => d.id === device.id).env_parameters}
                onChange={value => {
                    const newDevices = JSON.parse(JSON.stringify(instanceForm.devices));
                    newDevices.find(d => d.id === device.id).env_parameters = value;
                    setInstanceForm({
                        ...instanceForm,
                        devices: newDevices,
                    });
                }}
            />
        </div>}
        {checked && <div>
Options
            <ContainerOptions
                manifest={manifest}
                device={device}
                data={instanceForm.devices.find(d => d.id === device.id).parameters}
                onChange={value => {
                    const newDevices = JSON.parse(JSON.stringify(instanceForm.devices));
                    newDevices.find(d => d.id === device.id).parameters = value;
                    setInstanceForm({
                        ...instanceForm,
                        devices: newDevices,
                    });
                }}
            />
        </div>}
    </>;
};

export default InstanceDevice;
