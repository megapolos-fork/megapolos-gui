/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import { Button, TextField } from '@mui/material';
import Id from '../components/Id';

const Users = () => {
    const [appForm, setAppForm] = useState({ name: '' });
    const {
        loading, error, data, refetch,
    } = useQuery(gql`
        query {
            getUsers {
                id
                name
                group_user_id
                token
            }
        }
    `);
    const [createUser] = useMutation(gql`
        mutation($input: UserInput!) {
            addUser(input: $input)
        }
    `);
    console.log(loading, error, data);
    return (
        <>
            <h2>Users</h2>
            {error && 'Error!'}
            {loading && 'Loading...'}
            <h3>Add user</h3>
            <div>
                <TextField
                    label="Name"
                    value={appForm.name}
                    onChange={e => setAppForm({ ...appForm, name: e.target.value })}
                    variant="standard"
                />
            </div>
            <div>
                <Button
                    onClick={async () => {
                        await createUser({ variables: { input: appForm } });
                        refetch();
                    }}
                >
Add user
                </Button>
            </div>
            <table>
                <tr>
                    <td>id</td>
                    <td>name</td>
                    <td>role</td>
                    <td>token</td>
                </tr>
                {data?.getUsers.map(user => (
                    <tr key={user.id}>
                        <td><Id id={user.id} /></td>
                        <td>{user.name}</td>
                        <td>{user.group_user_id}</td>
                        <td><Id id={user.token} /></td>
                    </tr>
                ))}
            </table>
        </>
    );
};

export default Users;
