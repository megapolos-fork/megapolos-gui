/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import {
    Button, MenuItem, Select, TextField,
} from '@mui/material';
import { gql, useMutation, useQuery } from '@apollo/client';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import Id from '../components/Id';
import InstanceDevice from '../components/InstanceDevice';

const InstanceForm = props => {
    const {
        data,
    } = useQuery(gql`
        query {
            getDevices {
                id
                name
                device_type_id
            }
            getVolumes {
                id
                name
                type
                outer_path
            }
        }
    `);
    const [createInstance] = useMutation(gql`
        mutation($input: AppInstanceInput!) {
            createAppInstance(input: $input)
        }
    `);
    const { app, refetch } = props;
    const [instanceForm, setInstanceForm] = useState({
        app_id: app.id,
        name: '',
        containers: app.images.map(image => ({
            image_id: image.id,
            fixed_outer_port: 0,
            devices: [],
            volumes: [],
            envs: [],
        })),
    });

    return <>
        <TextField
            label="Instance name"
            value={instanceForm.name}
            onChange={e => setInstanceForm({ ...instanceForm, name: e.target.value })}
            variant="standard"
        />
        {instanceForm.containers.map((containerForm, containerIndex) => <div key={containerIndex}>
            <div>{app.images[containerIndex].name}</div>
            <TextField
                label="Fixed outer port"
                value={containerForm.fixed_outer_port}
                onChange={e => {
                    const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                    newInstanceForm.containers[containerIndex].fixed_outer_port = parseInt(e.target.value);
                    setInstanceForm(newInstanceForm);
                }}
                variant="standard"
            />
            {data?.getDevices?.map(device => <InstanceDevice
                key={device.id}
                device={device}
                instanceForm={containerForm}
                setInstanceForm={newImageForm => {
                    const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                    newInstanceForm.containers[containerIndex] = newImageForm;
                    setInstanceForm(newInstanceForm);
                }}
            />)}
            <div>Volumes</div>
            {containerForm.volumes.map((volumeForm, index) => {
                const volume = data?.getVolumes?.find(v => v.id === volumeForm.volume);
                return <div key={index}>
                    <TextField
                        label="Volume name"
                        value={volumeForm.name}
                        onChange={e => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].volumes[index].name = e.target.value;
                            setInstanceForm(newInstanceForm);
                        }}
                        variant="standard"
                    />
                    <TextField
                        label="Inner path"
                        value={volumeForm.inner_path}
                        onChange={e => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].volumes[index].inner_path = e.target.value;
                            setInstanceForm(newInstanceForm);
                        }}
                        variant="standard"
                    />
                    <Select
                        value={volumeForm.volume}
                        onChange={e => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].volumes[index].volume = e.target.value;
                            setInstanceForm(newInstanceForm);
                        }}
                        variant="standard"
                    >
                        <MenuItem value="">None</MenuItem>
                        {data?.getVolumes?.map(_volume => (
                            <MenuItem key={_volume.id} value={_volume.id}>{_volume.name}</MenuItem>
                        ))}
                    </Select>
                    {volume?.type}
                    <Button
                        onClick={() => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].volumes.splice(index, 1);
                            setInstanceForm(newInstanceForm);
                        }}
                    >
                    Delete
                    </Button>
                </div>;
            })}
            <Button onClick={() => {
                const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                newInstanceForm.containers[containerIndex].volumes.push({ name: '', inner_path: '', volume: '' });
                setInstanceForm(newInstanceForm);
            }}
            >
            Add volume
            </Button>
            <div>Envs</div>
            {containerForm.envs.map((envForm, index) => (
                <div key={index}>
                    <TextField
                        label="Key"
                        value={envForm.key}
                        onChange={e => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].envs[index].key = e.target.value;
                            setInstanceForm(newInstanceForm);
                        }}
                        variant="standard"
                    />
                    <TextField
                        label="Value"
                        value={envForm.value}
                        onChange={e => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].envs[index].value = e.target.value;
                            setInstanceForm(newInstanceForm);
                        }}
                        variant="standard"
                    />
                    <Button
                        onClick={() => {
                            const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                            newInstanceForm.containers[containerIndex].envs.splice(index, 1);
                            setInstanceForm(newInstanceForm);
                        }}
                    >
                    Delete
                    </Button>
                </div>
            ))}
            <Button
                onClick={() => {
                    const newInstanceForm = JSON.parse(JSON.stringify(instanceForm));
                    newInstanceForm.containers[containerIndex].envs.push({ key: '', value: '' });
                    setInstanceForm(newInstanceForm);
                }}
            >
Add env
            </Button>
        </div>)}
        <Button
            onClick={async () => {
                await createInstance({
                    variables:{
                        input: instanceForm,
                    },
                });
                refetch();
            }}
        >
Create instance
        </Button>
    </>;
};

const App = () => {
    const params = useParams();

    const { data, refetch } = useQuery(gql`
        query($id: String!) {
            getApp(id: $id) {
                id
                name
                owner_user_id
                images {
                    id
                    name
                    image
                    inner_port
                }
            }
        }
    `, {
        variables: {
            id: params.id,
        },
    });

    const [uninstallApp] = useMutation(gql`
        mutation($id: String!) {
            uninstallApp(id: $id)
        }
    `);

    const [createDeviceFromApp] = useMutation(gql`
        mutation($appId: String) {
            createDeviceFromApp(app_id: $appId)
        }
    `);

    if (!data) {
        return null;
    }

    return <div>
        <div>{data.getApp.name}</div>
        <table>
            <tr>
                <th>Id</th>
                <td><Id id={data.getApp.id} /></td>
            </tr>
            <tr>
                <th>owner_user_id</th>
                <td><Id id={data.getApp.owner_user_id} /></td>
            </tr>
        </table>
        <h4>Create Instance</h4>
        <InstanceForm app={data.getApp} refetch={refetch} />
        <h4>Actions</h4>
        <Button
            onClick={async () => {
                await createDeviceFromApp({ variables: { appId: data.getApp.id } });
                refetch();
            }}
        >
Create device
        </Button>
        <Button
            onClick={async () => {
                await uninstallApp({ variables: { id: data.getApp.id } });
                refetch();
            }}
        >
Uninstall
        </Button>
        <h4>Images</h4>
        <table>
            <tr key="header">
                <td>id</td>
                <td>name</td>
                <td>image</td>
                <td>inner_port</td>
            </tr>
            {data.getApp.images.map((image, index) => (
                <tr key={image.id || index}>
                    <td><Id id={image.id} /></td>
                    <td>{image.name}</td>
                    <td>{image.image}</td>
                    <td>{image.inner_port}</td>
                </tr>
            ))}
        </table>
    </div>;
};

export default App;
