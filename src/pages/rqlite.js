/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { Button } from '@mui/material';
import { useState } from 'react';

const Rqlite = () => {
    const [query, setQuery] = useState('');
    const [schema, setSchema] = useState({});
    const [data, setData] = useState({});
    const [result, setResult] = useState('');

    // console.log(Object.keys(schema).map(key => `interface ${key}Table {${schema[key].map(field => `${field[1]}: ${field[2]};`).join('\n')}}`).join('\n'));

    return <div>
        <div>
            <textarea value={query} onChange={e => setQuery(e.target.value)}></textarea>
            <Button
                onClick={async () => {
                    const rawResponse = await fetch('http://localhost:3000/db/query?pretty', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify([query]),
                    });
                    const content = await rawResponse.json();

                    setResult(JSON.stringify(content, null, 2));
                }}
            >
Query
            </Button>
            <Button
                onClick={async () => {
                    const rawResponse = await fetch('http://localhost:3000/db/execute?pretty', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify([query]),
                    });
                    const content = await rawResponse.json();

                    setResult(JSON.stringify(content, null, 2));
                }}
            >
Execute
            </Button>
        </div>
        <pre>{result}</pre>
        <div>
            <Button
                onClick={async () => {
                    const tablesResponse = await fetch('http://localhost:3000/db/query?pretty', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(['SELECT tbl_name FROM sqlite_master WHERE type=\'table\'']),
                    });
                    const tables = (await tablesResponse.json()).results[0].values;
                    const schemaData = {};
                    for (const table of tables) {
                        const schemaResponse = await fetch('http://localhost:3000/db/query?pretty', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify([`PRAGMA table_info(${table})`]),
                        });
                        const _schema = (await schemaResponse.json()).results[0].values;
                        schemaData[table] = _schema;
                    }
                    console.log(schemaData);
                    setSchema(schemaData);
                }}
            >
Show schema
            </Button>
            <Button onClick={() => setSchema({})}>Hide schema</Button>
        </div>
        {
            Object.keys(schema).map(table => <div key={table} style={{ display: 'inline-block' }}>
                <h3>{table}</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Type</th>
                            <th>NotNull</th>
                            <th>Default</th>
                            <th>PK</th>
                        </tr>
                    </thead>
                    <tbody>
                        {schema[table].map(field => <tr key={field.cid}>
                            <td>{field[1]}</td>
                            <td>{field[2]}</td>
                            <td>{field[3]}</td>
                            <td>{field[4]}</td>
                            <td>{field[5]}</td>
                        </tr>)}
                    </tbody>
                </table>
            </div>)
        }
        <div>
            <Button
                onClick={async () => {
                    const tablesResponse = await fetch('http://localhost:3000/db/query?pretty', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(['SELECT tbl_name FROM sqlite_master WHERE type=\'table\'']),
                    });
                    const tables = (await tablesResponse.json()).results[0].values;
                    const schemaData = {};
                    for (const table of tables) {
                        const schemaResponse = await fetch('http://localhost:3000/db/query?pretty', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify([`SELECT * FROM ${table}`]),
                        });
                        const _schema = (await schemaResponse.json()).results[0];
                        schemaData[table] = _schema;
                    }
                    console.log(schemaData);
                    setData(schemaData);
                }}
            >
Show data
            </Button>
            <Button onClick={() => setData({})}>Hide data</Button>
        </div>
        {
            Object.keys(data).map(table => <div key={table}>
                <h3>{table}</h3>
                <table>
                    <thead>
                        <tr>
                            {data[table].columns?.map(column => <th key={column}>{column}</th>)}
                        </tr>
                    </thead>
                    <tbody>
                        {data[table]?.values?.map(row => <tr key={row[0]}>
                            {row.map((cell, index) => <td key={index}>{cell}</td>)}
                        </tr>)}
                    </tbody>
                </table>
            </div>)
        }
    </div>;
};

export default Rqlite;
