/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import React, { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import {
    Accordion, AccordionDetails, AccordionSummary, Button, Checkbox, MenuItem, Select, TextField,
} from '@mui/material';
import { ExpandMore } from '@mui/icons-material';
import Id from '../components/Id';
import { readFileAsync } from './Volumes';

const Device = props => {
    const {
        data: manifest, refetch,
    } = useQuery(gql`
        query($id: String) {
            getDeviceManifest(id: $id) {
                fields
                container_aux_fields
                container_env_fields
            }
        }
    `, { variables: { id: props.device.id } });

    const [save] = useMutation(gql`
        mutation($id: String, $options: [DeviceOptionInput]) {
            setDeviceOptions(id: $id options: $options)
        }
    `);

    const deviceFields = {};
    props.device.options.forEach(option => {
        deviceFields[option.device_option_name] = option.device_option_value;
    });

    const [fields, setField] = useState(deviceFields);
    return manifest && <div>
        {manifest.getDeviceManifest.fields.map(field => <div key={field}>
            {field}
            {': '}
            <TextField
                value={fields[field]}
                onChange={e => {
                    const newFields = JSON.parse(JSON.stringify(fields));
                    newFields[field] = e.target.value;
                    setField(newFields);
                }}
                variant="standard"
            />
        </div>)}
        <Button onClick={async () => {
            await save({
                variables: {
                    id: props.device.id,
                    options: Object.keys(fields).map(field => ({
                        key: field,
                        value: fields[field],
                    })),
                },
            });
            refetch();
        }}
        >
Save
        </Button>
    </div>;
};

const Backup = props => {
    const [containerId, setContainerId] = useState('');

    const [restoreDeviceBackup] = useMutation(gql`
        mutation($deviceId: String, $containerId: String, $backupId: String) {
            restoreDeviceBackup(device_id: $deviceId container_id: $containerId backup_id: $backupId)
        }
    `);

    const [dowloadDeviceBackup] = useMutation(gql`
        mutation($backupId: String) {
            downloadDeviceBackup(backup_id: $backupId)
        }
    `);

    const containers = [];
    props.instances.forEach(instance => {
        instance.containers.forEach(container => {
            if (container.devices.find(device => device.device.id === props.device.id)) {
                containers.push(container);
            }
        });
    });

    return <div>
        <Id id={props.backup.id} />
        <div>
Name:
            {' '}
            {props.backup.name}
        </div>
        <div>
Container:
            {' '}
            {props.backup.container_id}
        </div>
        <div>
Image:
            {' '}
            {props.backup.image_id}
        </div>
        <div>
Date:
            {' '}
            {props.backup.create_date}
        </div>
        <div>
            <Select variant="standard" value={containerId} onChange={e => setContainerId(e.target.value)}>
                {containers.map(container => <MenuItem key={container.id} value={container.id}>{container.name}</MenuItem>)}
            </Select>
            <Button
                onClick={async () => {
                    await restoreDeviceBackup({
                        variables: {
                            deviceId: props.device.id,
                            backupId: props.backup.id,
                            containerId,
                        },
                    });
                    props.refetch();
                }}
            >
Restore
            </Button>
            <Button
                onClick={async () => {
                    await dowloadDeviceBackup({
                        variables: {
                            backupId: props.backup.id,
                        },
                        onCompleted: data => {
                            const a = document.createElement('a');
                            a.href = `data:application/zip;base64,${data.downloadDeviceBackup}`;
                            a.download = `${props.backup.name || props.backup.id}.zip`;
                            a.click();
                        },
                    });
                }}
            >
Download
            </Button>
            <Button
                onClick={async () => {
                    props.refetch();
                }}
            >
Remove
            </Button>
        </div>
    </div>;
};

const BackupVolume = props => {
    const [containerId, setContainerId] = useState('');

    const [volumeId, setVolumeId] = useState(props.backupVolume?.id);

    const [uploadForm, setUploadForm] = useState({ name: '', file: null, deviceId: props.device.id });

    const [uploadDeviceBackup] = useMutation(gql`
        mutation($deviceId: String, $name: String, $file: Upload!) {
            uploadDeviceBackup(device_id: $deviceId name: $name file: $file)
        }
    `);

    const [backupDevice] = useMutation(gql`
        mutation($deviceId: String, $containerId: String) {
            backupDevice(device_id: $deviceId container_id: $containerId)
        }
    `);

    const containers = [];
    props.instances.forEach(instance => {
        instance.containers.forEach(container => {
            if (container.devices.find(device => device.device.id === props.device.id)) {
                containers.push(container);
            }
        });
    });

    const {
        data, refetch,
    } = useQuery(gql`
        query($deviceName: String) {
            getDeviceBackups(device_name: $deviceName) {
                id
                name
                device_id
                device_name
                container_id
                image_id
                create_date
            }
        }
    `, { variables: { deviceName: props.device.name } });

    const [setDeviceBackupVolume] = useMutation(gql`
        mutation($deviceId: String, $volumeId: String) {
            setDeviceBackupVolume(device_id: $deviceId volume_id: $volumeId)
        }
    `);
    const [removeDeviceBackupVolume] = useMutation(gql`
        mutation($deviceId: String) {
            removeDeviceBackupVolume(device_id: $deviceId)
        }
    `);
    return <>
        {props.backupVolume ? <div>
            <Id id={props.backupVolume.id} name={props.backupVolume.name} />
            <div>{props.backupVolume.name}</div>
            <div>{props.backupVolume.outer_path}</div>
            <Button
                onClick={async () => {
                    await removeDeviceBackupVolume({ variables: { deviceId: props.device.id } });
                    props.refetch();
                }}
            >
            Remove backup volume
            </Button>
            <h3>Backups</h3>
            {data ? data.getDeviceBackups.map(backup => <Backup
                key={backup.id}
                backup={backup}
                device={props.device}
                refetch={props.refetch}
                instances={props.instances}
            />) : null}
            <div>
                <Select variant="standard" value={containerId} onChange={e => setContainerId(e.target.value)}>
                    {containers.map(container => <MenuItem key={container.id} value={container.id}>{container.name}</MenuItem>)}
                </Select>
                <Button
                    onClick={async () => {
                        await backupDevice({
                            variables: {
                                deviceId: props.device.id,
                                containerId,
                            },
                        });
                        props.refetch();
                    }}
                >
Backup
                </Button>
            </div>
            <h3>Upload backup</h3>
            <TextField value={uploadForm.name} onChange={e => setUploadForm({ ...uploadForm, name: e.target.value })} variant="standard" />
            <input
                type="file"
                onChange={async e => {
                    const file = e.target.files[0];
                    const fileBase64 = await readFileAsync(file);
                    setUploadForm({
                        ...uploadForm,
                        file: {
                            filename: fileBase64.filename,
                            data: fileBase64.data,
                        },
                    });
                }}
            />
            <Button
                onClick={async () => {
                    await uploadDeviceBackup({
                        variables: uploadForm,
                    });
                    refetch();
                }}
            >
Upload
            </Button>
        </div> : null}
        <Select
            value={volumeId}
            onChange={e => setVolumeId(e.target.value)}
            variant="standard"
        >
            {props.getVolumes.map(volume => <MenuItem key={volume.id} value={volume.id}>{volume.name}</MenuItem>)}
        </Select>
        <Button
            onClick={async () => {
                await setDeviceBackupVolume({ variables: { deviceId: props.device.id, volumeId } });
                props.refetch();
            }}
        >
            Set backup volume
        </Button>
    </>;
};

const VirtualDevice = props => {
    const [form, setForm] = useState({
        isVirtual: props.device.is_virtual,
        virtualDeviceContainerId: props.device.virtual_device_container_id,
    });

    const [setDeviceVirtual] = useMutation(gql`
        mutation($id: String, $isVirtual: Int, $virtualDeviceContainerId: String) {
            setDeviceVirtual(id: $id is_virtual: $isVirtual virtual_device_container_id: $virtualDeviceContainerId)
        }
    `);

    const containers = props.instances.map(instance => instance.containers).flat();

    return <div>
        <h3>Virtual device</h3>
        <div>
            <Checkbox
                checked={!!form.isVirtual}
                onChange={e => setForm({ ...form, isVirtual: e.target.checked ? 1 : 0 })}
            />
            {form.isVirtual ? <Select variant="standard" value={form.virtualDeviceContainerId} onChange={e => setForm({ ...form, virtualDeviceContainerId: e.target.value })}>
                {containers.map(container => <MenuItem key={container.id} value={container.id}>{container.name}</MenuItem>)}
            </Select> : null}
        </div>
        <Button
            onClick={async () => {
                await setDeviceVirtual({ variables: { id: props.device.id, ...form } });
                props.refetch();
            }}
        >
            Save
        </Button>
    </div>;
};

const Devices = () => {
    const [deviceForm, setAppForm] = useState({
        name: '', image: '', inner_port: 0,
    });
    const {
        loading, error, data, refetch,
    } = useQuery(gql`
        query {
            getDevices {
                id
                name
                device_type_id
                backup_volume_id
                is_virtual
                virtual_device_container_id
                options {
                    device_id
                    device_option_name
                    device_option_value
                  }
            }
            getVolumes {
                id
                name
                outer_path
            }
            getAppInstances {
                containers {
                    id
                    name
                    devices {
                        device {
                            id
                        }
                    }
                }
            }
        }
    `);
    const [addDevice] = useMutation(gql`
        mutation($input: DeviceInput) {
            addDevice(input: $input)
        }
    `);
    const [removeDevice] = useMutation(gql`
        mutation($id: String){
            removeDevice(id: $id)
        }
    `);
    if (data) {
        // data.getDevices.forEach(async device => {
        //     console.log(await fetchApi('/devices/app_options_env/get_fields', { id: device.id }));
        // });
    }
    return (
        <>
            <h2>Apps</h2>
            {error && 'Error!'}
            {loading && 'Loading...'}
            <h3>Add device</h3>
            <div>
                <TextField
                    label="Name"
                    value={deviceForm.name}
                    onChange={e => setAppForm({ ...deviceForm, name: e.target.value })}
                    variant="standard"
                />
            </div>
            <div>
                <TextField
                    label="Image"
                    value={deviceForm.image}
                    onChange={e => setAppForm({ ...deviceForm, image: e.target.value })}
                    variant="standard"
                />
            </div>
            <div>
                <TextField
                    type="number"
                    label="Inport"
                    value={deviceForm.inner_port}
                    onChange={e => setAppForm({ ...deviceForm, inner_port: parseInt(e.target.value) })}
                    variant="standard"
                />
            </div>
            <div>
                <Button
                    onClick={async () => {
                        await addDevice({ variables: { input: deviceForm } });
                        refetch();
                    }}
                >
Install
                </Button>
            </div>

            {data?.getDevices.map((device, index) => {
                const backupVolume = data.getVolumes.find(volume => volume.id === device.backup_volume_id);
                return <React.Fragment key={device.id || index}>
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMore />}>
                            {device.name}
                        </AccordionSummary>
                        <AccordionDetails>
                            <table>
                                <tr>
                                    <td>id</td>
                                    <td><Id id={device.id} /></td>
                                </tr>
                                <tr>
                                    <td>name</td>
                                    <td>{device.name}</td>
                                </tr>
                                <tr>
                                    <td>device_type_id</td>
                                    <td>{device.device_type_id}</td>
                                </tr>
                            </table>
                            <h3>Fields</h3>
                            <Device device={device} />
                            <VirtualDevice device={device} instances={data.getAppInstances} refetch={refetch} />
                            <h3>Backup volume</h3>
                            <BackupVolume device={device} backupVolume={backupVolume} getVolumes={data.getVolumes} refetch={refetch} instances={data.getAppInstances} />
                            <Button
                                onClick={async () => {
                                    await removeDevice({ variables: { id: device.id } });
                                    refetch();
                                }}
                            >
Remove device
                            </Button>
                        </AccordionDetails>
                    </Accordion>
                </React.Fragment>;
            })}
        </>
    );
};

export default Devices;
