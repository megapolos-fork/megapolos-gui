/* License: Apache 2.0. https://www.apache.org/licenses/LICENSE-2.0 */

import { gql, useMutation, useQuery } from '@apollo/client';
import { Button } from '@mui/material';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import Id from '../components/Id';
import DbDialog from '../dialogs/DbDialog';
import DomainDialog from '../dialogs/DomainDialog';
import CertificateDialog from '../dialogs/CertificateDialog';
import BuilderDialog from '../dialogs/BuilderDialog';
import RepositoryDialog from '../dialogs/RepositoryDialog';
import EnvsDialog from '../dialogs/EnvsDialog';

const DeviceFormNew = props => {
    const [dialog, setDialog] = useState(false);
    return <>
        {props.device.name}
        <Button onClick={() => setDialog(true)}>{props.update ? 'Edit device' : 'Add device'}</Button>
        {props.device.device_type_id === 'db' && <DbDialog
            open={dialog}
            onClose={() => setDialog(false)}
            containerId={props.container_id}
            deviceId={props.device.id}
            update={props.update}
        />}
        {props.device.device_type_id === 'domain' && <DomainDialog
            open={dialog}
            onClose={() => setDialog(false)}
            containerId={props.container_id}
            deviceId={props.device.id}
            update={props.update}
        />}
        {props.device.device_type_id === 'certificate' && <CertificateDialog
            open={dialog}
            onClose={() => setDialog(false)}
            containerId={props.container_id}
            deviceId={props.device.id}
            update={props.update}
        />}
        {props.device.device_type_id === 'builder' && <BuilderDialog
            open={dialog}
            onClose={() => setDialog(false)}
            containerId={props.container_id}
            deviceId={props.device.id}
            update={props.update}
        />}
        {props.device.device_type_id === 'repository' && <RepositoryDialog
            open={dialog}
            onClose={() => setDialog(false)}
            containerId={props.container_id}
            deviceId={props.device.id}
            update={props.update}
        />}
    </>;
};

const DevicesFormNew = props => {
    console.log(props);
    const {
        data,
    } = useQuery(gql`
        query {
            getDevices {
                id
                name
                device_type_id
            }
        }
    `);

    const ids = props.devices?.map(device => device.device.id);

    if (!data) return null;
    return data.getDevices.map(device => <div key={device.id}>
        <DeviceFormNew
            container_id={props.container_id}
            device={device}
            update={ids?.includes(device.id)}
        />
    </div>);
};

const Instance = () => {
    const params = useParams();

    const {
        loading, error, data, refetch,
    } = useQuery(gql`
        query($id: String!) {
            getAppInstance(id: $id) {
                id
                app_id
                user_id
                life_status
                name
                containers {
                    id
                    docker_runtime_id
                    name
                    life_status
                    docker_status
                    outer_port
                    devices {
                        device {
                            id
                            name
                        }
                        env_parameters {
                            key
                            value
                        }
                        # parameters {
                        #     key
                        #     value
                        # }
                    }
                    volumes {
                        id
                        volume_id
                        name
                        inner_path
                    }
                    envs {
                        key
                        value
                    }
                }
            }
            getApps {
                id
                name
            }
            getVolumes {
                id
                name
                type
                outer_path
            }
        }
    `, { variables: { id: params.id } });

    const [startInstance] = useMutation(gql`
        mutation($id: String!) {
            startAppInstance(id: $id)
        }
    `);
    const [stopInstance] = useMutation(gql`
        mutation($id: String!) {
            stopAppInstance(id: $id)
        }
    `);
    const [removeInstance] = useMutation(gql`
        mutation($id: String!) {
            removeAppInstance(id: $id)
        }
    `);
    const [updateContainer] = useMutation(gql`
        mutation($id: String! $noRebuild: Boolean) {
            updateContainer(id: $id noRebuild: $noRebuild)
        }
    `);
    const [removeDevice] = useMutation(gql`
        mutation($containerId: String, $deviceId: String) {
            removeDeviceFromContainer(container_id: $containerId device_id: $deviceId)
        }
    `);
    if (!data) {
        return null;
    }
    const app = data.getApps.find(_app => _app.id === data.getAppInstance.app_id);

    return <div>
        {error && 'Error!'}
        {loading && 'Loading...'}
        <div>
            {app?.name}
            {': '}
            {data.getAppInstance.name}
            {' | '}
            {data.getAppInstance.containers.map(container => `${container.life_status} | ${container.docker_status}`).join(', ')}
        </div>
        <div>
            <table>
                <tr>
                    <th>id</th>
                    <td><Id id={data.getAppInstance.id} /></td>
                </tr>
                <tr>
                    <th>app</th>
                    <td><Id id={data.getAppInstance.app_id} /></td>
                </tr>
                <tr>
                    <th>user</th>
                    <td><Id id={data.getAppInstance.user_id} /></td>
                </tr>
                <tr>
                    <th>life status</th>
                    <td>{data.getAppInstance.life_status}</td>
                </tr>
            </table>
            <h4>Actions</h4>
            <div>
                {data.getAppInstance.life_status === 'running' && <Button
                    onClick={async () => {
                        await stopInstance({ variables: { id: data.getAppInstance.id } });
                        refetch();
                    }}
                >
Stop
                </Button>}
                {data.getAppInstance.life_status === 'stopped' && <Button
                    onClick={async () => {
                        await startInstance({ variables: { id: data.getAppInstance.id } });
                        refetch();
                    }}
                >
Start
                </Button>}
                <Button
                    onClick={async () => {
                        await removeInstance({ variables: { id: data.getAppInstance.id } });
                        refetch();
                    }}
                >
Remove
                </Button>
            </div>
            <h4>Containers</h4>
            <table>
                <tr key="header">
                    <td>id</td>
                    <td>docker_runtime_id</td>
                    <td>name</td>
                    <td>life_status</td>
                    <td>docker_status</td>
                    <td>url</td>
                    <td>devices</td>
                    <td>volumes</td>
                    <td>envs</td>
                    <td>actions</td>
                </tr>
                {data.getAppInstance.containers.map((container, _index) => (
                    <tr key={container.id || _index}>
                        <td><Id id={container.id} /></td>
                        <td><Id id={container.docker_runtime_id} /></td>
                        <td>{container.name}</td>
                        <td>{container.life_status}</td>
                        <td>{container.docker_status}</td>
                        <td><a href={`http://localhost:${container.outer_port}/`}>{container.outer_port}</a></td>
                        <td>
                            {container.devices?.map(containerDevice => <div>
                                <div>{containerDevice.device.name}</div>
                                <div>
                                    {containerDevice.env_parameters?.map(env => <div>
                                        {env.key}
                                        {': '}
                                        {env.value}
                                    </div>)}
                                </div>
                                <div>
                                    {containerDevice.parameters?.map(param => <div>
                                        {param.key}
                                        {': '}
                                        {param.value}
                                    </div>)}
                                </div>
                                <div>
                                    <Button
                                        onClick={async () => {
                                            await removeDevice({ variables: { containerId: container.id, deviceId: containerDevice.device.id } });
                                            refetch();
                                        }}
                                    >
Remove
                                    </Button>
                                </div>
                            </div>)}
                            <DevicesFormNew app={app} refetch={refetch} devices={container.devices} container_id={container.id} />
                        </td>
                        <td>
                            {container.volumes?.map(containerVolume => {
                                const volume = data.getVolumes.find(_volume => _volume.id === containerVolume.volume_id);
                                return <div key={containerVolume.id}>
                                    <div>{containerVolume.name}</div>
                                    <table>
                                        <tr>
                                            <th>id</th>
                                            <td><Id id={containerVolume.id} /></td>
                                        </tr>
                                        <tr>
                                            <th>inner path</th>
                                            <td>{containerVolume.inner_path}</td>
                                        </tr>
                                        <tr>
                                            <th>volume name</th>
                                            <td>{volume?.name}</td>
                                        </tr>
                                    </table>
                                </div>;
                            })}
                        </td>
                        <td>
                            <table>
                                {container.envs?.map(containerEnv => <tr key={containerEnv.key}>
                                    <td>{containerEnv.key}</td>
                                    <td>{containerEnv.value}</td>
                                </tr>)}
                                <EnvsDialog refetch={refetch} containerId={container.id} envs={container.envs} />
                            </table>
                        </td>
                        <td>
                            <Button
                                onClick={async () => {
                                    await updateContainer({ variables: { id: container.id, noRebuild: false } });
                                    refetch();
                                }}
                            >
Update
                            </Button>
                            <Button
                                onClick={async () => {
                                    await updateContainer({ variables: { id: container.id, noRebuild: true } });
                                    refetch();
                                }}
                            >
Update without rebuild
                            </Button>
                        </td>
                    </tr>
                ))}
            </table>
        </div>
    </div>;
};

export default Instance;
