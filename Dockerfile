FROM node:14 as builder
WORKDIR /app
RUN npm install -g serve
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

FROM nginx:latest AS front
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
